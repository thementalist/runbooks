# Update GitLab Runner on runners managers

This runbook describes procedure of upgrading GitLab Runner on our runner managers.

## Roles to runners mapping

```mermaid
graph LR
    r::base(gitlab-runner-base)
    r::builder(gitlab-runner-builder)

    r::gsrm(gitlab-runner-gsrm)
    r::gsrm-do(gitlab-runner-gsrm-do)
    r::gsrm-gce(gitlab-runner-gsrm-gce)
    r::gsrm-gce-us-east1-c(gitlab-runner-gsrm-gce-us-east1-c)
    r::gsrm-gce-us-east1-d(gitlab-runner-gsrm-gce-us-east1-d)

    r::prm(gitlab-runner-prm)
    r::prm-do(gitlab-runner-prm-do)

    r::srm(gitlab-runner-srm)
    r::srm-do(gitlab-runner-srm-do)
    r::srm-gce(gitlab-runner-srm-gce)
    r::srm-gce-us-east1-c(gitlab-runner-srm-gce-us-east1-c)
    r::srm-gce-us-east1-d(gitlab-runner-srm-gce-us-east1-d)

    r::stg-srm(gitlab-runner-stg-srm)
    r::stg-srm-gce(gitlab-runner-stg-srm-gce)
    r::stg-srm-gce-us-east1-c(gitlab-runner-stg-srm-gce-us-east1-c)
    r::stg-srm-gce-us-east1-d(gitlab-runner-stg-srm-gce-us-east1-d)

    n::grb[gitlab-runner-builder.gitlap.com]

    n::gsrm1[gitlab-shared-runners-manager-1.gitlab.com]
    n::gsrm2[gitlab-shared-runners-manager-2.gitlab.com]
    n::gsrm3[gitlab-shared-runners-manager-3.gitlab.com]
    n::gsrm4[gitlab-shared-runners-manager-4.gitlab.com]

    n::prm1[private-runners-manager-1.gitlab.com]
    n::prm2[private-runners-manager-2.gitlab.com]

    n::srm1[shared-runners-manager-1.gitlab.com]
    n::srm2[shared-runners-manager-2.gitlab.com]
    n::srm3[shared-runners-manager-3.gitlab.com]
    n::srm4[shared-runners-manager-4.gitlab.com]

    n::srm3::stg[shared-runners-manager-3.staging.gitlab.com]
    n::srm4::stg[shared-runners-manager-4.staging.gitlab.com]

    r::base --> r::builder
    r::builder ==> n::grb

    r::base --> r::gsrm
    r::gsrm --> r::gsrm-do
    r::gsrm-do ==> n::gsrm1
    r::gsrm-do ==> n::gsrm2
    r::gsrm --> r::gsrm-gce
    r::gsrm-gce --> r::gsrm-gce-us-east1-c
    r::gsrm-gce-us-east1-c ==> n::gsrm4
    r::gsrm-gce --> r::gsrm-gce-us-east1-d
    r::gsrm-gce-us-east1-d ==> n::gsrm3

    r::base --> r::prm
    r::prm --> r::prm-do
    r::prm-do ==> n::prm1
    r::prm-do ==> n::prm2

    r::base --> r::srm
    r::srm --> r::srm-do
    r::srm-do ==> n::srm1
    r::srm-do ==> n::srm2
    r::srm --> r::srm-gce
    r::srm-gce --> r::srm-gce-us-east1-c
    r::srm-gce-us-east1-c ==> n::srm4
    r::srm-gce --> r::srm-gce-us-east1-d
    r::srm-gce-us-east1-d ==> n::srm3

    r::srm --> r::stg-srm
    r::srm-gce --> r::stg-srm-gce
    r::stg-srm --> r::stg-srm-gce
    r::srm-gce-us-east1-c --> r::stg-srm-gce-us-east1-c
    r::stg-srm-gce --> r::stg-srm-gce-us-east1-c
    r::stg-srm-gce-us-east1-c ==> n::srm4::stg
    r::srm-gce-us-east1-d --> r::stg-srm-gce-us-east1-d
    r::stg-srm-gce --> r::stg-srm-gce-us-east1-d
    r::stg-srm-gce-us-east1-d ==> n::srm3::stg
```

## Requirements

To upgrade runners on managers you need to:

- have write access to dev.gitlab.org/cookbooks/chef-repo,
- have write access to chef.gitlab.com,
- have configured knife environment,
- have admin access to nodes (sudo access).

## Procedure description

> **Notice**: to make update process transparent for users we should update one runner's host
> at a time. For example GitLab CE project on GitLab.com is using four runners: gitlab-shared-runners-manager-1,
> gitlab-shared-runners-manager-2 (as a shared runners), and both private-runners-manager-X (as specific runners).
>
> If we want to update private-runners-manager-X we should first update private-runners-manager-1, and after this
> update the private-runners-manager-2. It needs to be done like this because of Runner's graceful stop process -
> Runner needs time to finish running builds and during this time it will not handle new builds.
>
> Because of this updating all Runners at once could block jobs processing even for two hours!

1. **Shutdown `chef-client` process on managers being updated**

    For example, to shutdown chef-client on private-runners-manager-X.gitlab.com, you can execute:

    ```bash
    $ knife ssh -aipaddress 'roles:gitlab-runner-prm' -- sudo service chef-client stop
    ```

    To be sure that chef-cilent process is terminated you can execute:

    ```bash
    $ knife ssh -aipaddress 'roles:gitlab-runner-prm' -- "service chef-client status; ps aux | grep chef"
    ```

1. **Update chef role (or roles)**

    > **Notice:** This needs to be done only onece if you are updating few nodes using the same role.

    In `chef-repo` directory execute:

    ```bash
    $ rake edit_role[gitlab-runner-prm]
    ```

    where `gitlab-runner-prm` is a role used by nodes that you are updating. Please check the
    [roles to runners mapping section](#roles-to-runners-mapping) to find which role you're interested in.

    In attributes list look for `cookbook-gitlab-runner:gitlab-runner:version` and change it to a version that you want
    to update. It should look like:

    ```json
    "cookbook-gitlab-runner": {
      "gitlab-runner": {
        "repository": "gitlab-runner",
        "version": "10.4.0"
      }
    }
    ```

    If you want to install a Bleeding Edge version of the Runner, you should set the `repository`
    value to `unstable`.

    If you want to install a Stable version of the Runner, you should set the `repository` value to
    `gitlab-runner` (which is a default if the key doesn't exists in configuration).

1. **Upgrade all GitLab Runners**

    To upgrade chosen Runners manager, execute the command:

    ```bash
    $ knife ssh -C1 -aipaddress 'roles:gitlab-runner-prm' -- sudo /root/runner_upgrade.sh
    ```

    This will send a stop signal to the Runner. The process will wait until all handled jobs are finished,
    but no longer than 7200 seconds. The `-C1` flag will make sure that only one node using chosen role
    will be updated at a time.

    When the last job will be finished, or after the 7200 seconds timeout, the process will
    be terminated and the script will:
    - remove all Docker Machines that were created by Runner
      (using the `/root/machines_operations.sh remove-all` script),
    - upgrade Runner and configuration with `chef-client` (which will also start the `chef-client` process
      stopped in the first step of the upgrade process),
    - start Runner's process and check if process is running,
    - show the output of `gitlab-runner --version`.

    When upgrade of the first Runner is done, then continue with another one.

1. **Verify the version of GitLab Runner**

    If you want to check which version of Runner is installed, execute the following command:

    ```bash
    $ knife ssh -aipaddress 'roles:gitlab-runner-prm' -- gitlab-runner --version
    ```

    You can also check the [uptime](https://performance.gitlab.net/dashboard/db/ci?refresh=5m&orgId=1&panelId=18&fullscreen)
    and [version](https://performance.gitlab.net/dashboard/db/ci?refresh=5m&orgId=1&panelId=12&fullscreen) on
    CI dashboard at https://performance.gitlab.net/. Notice that the version table shows versions existing for last 1
    minute so if you check it immediately after upgrading Runner you may see it twice - with old and new version.
    After a minute the old entry should disappear.

1. **Update GitLab.com's configuration description**

    If you are updating shared runners used by GitLab.com, please create a merge request in
    https://gitlab.com/gitlab-com/www-gitlab-com project to update Runner's version and/or any other changed
    configuration values which are specified at
    https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/source/gitlab-com/settings/index.html.md#shared-runners.

## Upgrade of whole GitLab.com Runners fleet

We're in the process of refactorizing configuration of GitLab.com's Runners. Currently, if you want to update
the version on all Runners, it's easiest to edit `gitlab-runner-base` role. If you want to update only selected
Runner, then you should edit a related role, and set chosen version with `override_attributes`.

If you want to upgrade all Runners of GitLab.com fleet at the same time, then you can use the following script:

```bash
# Stop chef-client
knife ssh -aipaddress 'roles:gitlab-runner-base' -- sudo service chef-client stop

# Update configuration in roles definition and secrets
rake edit_role[gitlab-runner-base]

# Upgrade Runner's version and configuration on nodes
knife ssh -C1 -aipaddress 'roles:gitlab-runner-builder' -- sudo /root/runner_upgrade.sh &
knife ssh -C1 -aipaddress 'roles:gitlab-runner-gsrm' -- sudo /root/runner_upgrade.sh &
knife ssh -C1 -aipaddress 'roles:gitlab-runner-prm' -- sudo /root/runner_upgrade.sh &
knife ssh -C1 -aipaddress 'roles:gitlab-runner-srm' -- sudo /root/runner_upgrade.sh &
wait
```
